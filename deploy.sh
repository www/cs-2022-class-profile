#!/usr/bin/env bash

set -ex
ulimit -u 512

DIR=$(mktemp --directory)
trap "rm -rf $DIR" EXIT

pushd $DIR

git clone file:///srv/git/www/cs-2022-class-profile.git --depth=1
cd cs-2022-class-profile

export NEXT_PUBLIC_BASE_PATH="/classprofile/2022"
npm install
npm run build
npm run export

chgrp -R www out
chmod -R g+w out

shopt -s dotglob

rm -rf /srv/classprofile/2022/*
mv out/* /srv/classprofile/2022/

popd