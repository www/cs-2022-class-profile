import {
  mockBoxPlotData,
  mockCategoricalData,
  mockLineData,
  moreMockCategoricalData,
} from "data/mocks";
import { pageRoutes } from "data/routes";
import React from "react";
import { Color } from "utils/Color";
import { useWindowDimensions } from "utils/getWindowDimensions";
import { useIsMobile } from "utils/isMobile";

import { BarGraphVertical, BarGraphHorizontal } from "@/components/BarGraph";
import { BottomNav } from "@/components/BottomNav";
import { BoxPlot } from "@/components/Boxplot";
import { ComponentSwitcher } from "@/components/ComponentSwitcher";
import { ComponentWrapper } from "@/components/ComponentWrapper";
import { Header } from "@/components/Header";
import { LineGraph } from "@/components/LineGraph";
import { SectionHeader } from "@/components/SectionHeader";
import { SectionWrapper } from "@/components/SectionWrapper";
import { WordCloud } from "@/components/WordCloud";

import styles from "./samplePage.module.css";

export default function SamplePage() {
  const { width } = useWindowDimensions();
  const isMobile = useIsMobile();

  // For components that are in the side wrappers, it looks better if they fill a certain amount of width, so we can make the width dynamic like this
  const defaultGraphWidth = isMobile ? width / 1.25 : width / 2;
  const defaultGraphHeight = 500;

  // Make vars for common configs such as common margins
  const defaultVerticalBarGraphMargin = {
    top: 20,
    bottom: 80,
    left: 60,
    right: 20,
  };
  const defaultHorizontalBarGraphMargin = {
    top: 20,
    bottom: 80,
    left: 120,
    right: 20,
  };

  return (
    <div className={styles.page}>
      <Header />
      <SectionWrapper title="Transfer" />
      <SectionHeader
        title="Demographics"
        subtitle="An insight into the demographics of UW’s CS programs"
      />
      <ComponentWrapper
        heading="TESTING?"
        bodyText="There are a total of 106 respondents of the CS Class Profile. Interestingly, there are a huge number of students that are just in CS, partially due to the overwhelming number of people in CS as seen in the total demographics."
        align="right"
        noBackground
      >
        <ComponentSwitcher
          graphList={[
            <BarGraphHorizontal
              className={styles.barGraphDemo}
              data={mockCategoricalData}
              width={defaultGraphWidth}
              height={defaultGraphHeight}
              margin={defaultHorizontalBarGraphMargin}
              key={1}
            />,
            <LineGraph
              data={mockLineData}
              width={defaultGraphWidth}
              height={417}
              margin={{
                top: 20,
                bottom: 80,
                left: 30,
                right: 20,
              }}
              colorRange={[Color.primaryAccent, Color.secondaryAccentLight]}
              key={1}
            />,
            <BarGraphHorizontal
              className={styles.barGraphDemo}
              data={mockCategoricalData}
              width={defaultGraphWidth}
              height={defaultGraphHeight}
              margin={defaultHorizontalBarGraphMargin}
              key={1}
            />,
            <LineGraph
              data={mockLineData}
              width={defaultGraphWidth}
              height={417}
              margin={{
                top: 20,
                bottom: 80,
                left: 30,
                right: 20,
              }}
              colorRange={[Color.primaryAccent, Color.secondaryAccentLight]}
              key={1}
            />,
            <BarGraphHorizontal
              className={styles.barGraphDemo}
              data={mockCategoricalData}
              width={defaultGraphWidth}
              height={defaultGraphHeight}
              margin={defaultHorizontalBarGraphMargin}
              key={1}
            />,
            <LineGraph
              data={mockLineData}
              width={defaultGraphWidth}
              height={417}
              margin={{
                top: 20,
                bottom: 80,
                left: 30,
                right: 20,
              }}
              colorRange={[Color.primaryAccent, Color.secondaryAccentLight]}
              key={1}
            />,
            <BarGraphHorizontal
              className={styles.barGraphDemo}
              data={mockCategoricalData}
              width={defaultGraphWidth}
              height={defaultGraphHeight}
              margin={defaultHorizontalBarGraphMargin}
              key={1}
            />,
            <LineGraph
              data={mockLineData}
              width={defaultGraphWidth}
              height={417}
              margin={{
                top: 20,
                bottom: 80,
                left: 30,
                right: 20,
              }}
              colorRange={[Color.primaryAccent, Color.secondaryAccentLight]}
              key={1}
            />,
          ]}
          buttonList={["1A", "1B", "2A", "2B", "3A", "3B", "4A", "4B"]}
        />
      </ComponentWrapper>
      <ComponentWrapper
        heading="What program are you in?"
        bodyText="There are a total of 106 respondents of the CS Class Profile. Interestingly, there are a huge number of students that are just in CS, partially due to the overwhelming number of people in CS as seen in the total demographics."
      >
        <BarGraphVertical
          data={mockCategoricalData}
          width={defaultGraphWidth}
          height={defaultGraphHeight}
          margin={defaultVerticalBarGraphMargin}
        />
      </ComponentWrapper>
      <ComponentWrapper
        heading="What program are you in?"
        bodyText="There are a total of 106 respondents of the CS Class Profile. Interestingly, there are a huge number of students that are just in CS, partially due to the overwhelming number of people in CS as seen in the total demographics."
        align="center"
        noBackground
      >
        <WordCloud
          data={moreMockCategoricalData.map((word) => ({
            text: word.key,
            value: word.value,
          }))}
          // For components that we don't want to match the width necessarily we can provide direct values
          width={defaultGraphWidth}
          height={defaultGraphHeight}
        />
      </ComponentWrapper>
      <ComponentWrapper heading="What program are you in?" align="right">
        <BarGraphHorizontal
          className={styles.barGraphDemo}
          data={mockCategoricalData}
          width={defaultGraphWidth}
          height={defaultGraphHeight}
          // You can override specific margins if needed
          margin={defaultHorizontalBarGraphMargin}
        />
      </ComponentWrapper>
      <ComponentWrapper
        heading="What program are you in?"
        bodyText="There are a total of 106 respondents of the CS Class Profile. Interestingly, there are a huge number of students that are just in CS, partially due to the overwhelming number of people in CS as seen in the total demographics."
        align="left"
        noBackground
      >
        <BarGraphHorizontal
          className={styles.barGrapDemo}
          data={mockCategoricalData}
          width={defaultGraphWidth}
          height={defaultGraphHeight}
          margin={defaultHorizontalBarGraphMargin}
        />
      </ComponentWrapper>
      <ComponentWrapper heading="What program are you in?" align="left">
        <BarGraphHorizontal
          className={styles.barGraphDemo}
          data={mockCategoricalData}
          width={defaultGraphWidth}
          height={defaultGraphHeight}
          margin={defaultHorizontalBarGraphMargin}
        />
      </ComponentWrapper>
      <ComponentWrapper
        heading="What program are you in?"
        bodyText="There are a total of 106 respondents of the CS Class Profile. Interestingly, there are a huge number of students that are just in CS, partially due to the overwhelming number of people in CS as seen in the total demographics."
        align="left"
        noBackground
      >
        <WordCloud
          data={moreMockCategoricalData.map((word) => ({
            text: word.key,
            value: word.value,
          }))}
          width={defaultGraphWidth}
          height={defaultGraphHeight}
        />
      </ComponentWrapper>
      <ComponentWrapper heading="What program are you in? " align="right">
        <WordCloud
          data={moreMockCategoricalData.map((word) => ({
            text: word.key,
            value: word.value,
          }))}
          width={defaultGraphWidth}
          height={defaultGraphHeight}
        />
      </ComponentWrapper>
      <ComponentWrapper
        heading="What program are you in?"
        align="center"
        noBackground
      >
        <WordCloud
          data={moreMockCategoricalData.map((word) => ({
            text: word.key,
            value: word.value,
          }))}
          width={defaultGraphWidth}
          height={defaultGraphHeight}
        />
      </ComponentWrapper>
      <ComponentWrapper heading="What program are you in?" align="left">
        <WordCloud
          data={moreMockCategoricalData.map((word) => ({
            text: word.key,
            value: word.value,
          }))}
          width={defaultGraphWidth}
          height={defaultGraphHeight}
        />
      </ComponentWrapper>
      <ComponentWrapper
        heading="What program are you in?"
        bodyText="There are a total of 106 respondents of the CS Class Profile. Interestingly, there are a huge number of students that are just in CS, partially due to the overwhelming number of people in CS as seen in the total demographics."
        align="left"
      >
        <BarGraphHorizontal
          className={styles.barGraphDemo}
          data={mockCategoricalData}
          width={defaultGraphWidth}
          height={defaultGraphHeight}
          margin={defaultHorizontalBarGraphMargin}
        />
      </ComponentWrapper>
      <ComponentWrapper
        heading="What program are you in?"
        align="center"
        noBackground
      >
        <BoxPlot
          width={600}
          height={400}
          data={mockBoxPlotData}
          margin={{
            top: 20,
            left: 20,
          }}
        />
      </ComponentWrapper>
      <ComponentWrapper
        heading="What program are you in?"
        align="right"
        bodyText="Pariatur deserunt aute laborum ea adipisicing. Labore labore ipsum duis nisi ea incididunt ipsum occaecat. Ut occaecat et exercitation incididunt sit sit duis deserunt velit culpa nisi et dolore."
      >
        <LineGraph
          data={mockLineData}
          width={600}
          height={400}
          margin={{
            top: 20,
            bottom: 80,
            left: 30,
            right: 20,
          }}
          colorRange={[Color.primaryAccent, Color.secondaryAccentLight]}
        />
      </ComponentWrapper>
      <BottomNav
        leftPage={pageRoutes.demographics}
        rightPage={pageRoutes.contributors}
      ></BottomNav>
    </div>
  );
}
