import { BarGraphHorizontal, BarGraphVertical } from "components/BarGraph";
import { BoxPlot } from "components/Boxplot";
import {
  mockCategoricalData,
  moreMockCategoricalData,
  mockStackedBarKeys,
  mockStackedBarGraphData,
  mockBoxPlotData,
  mockLineData,
  mockQuoteData,
  mockQuoteDataLong,
  mockPieData,
  mockTimelineData,
  mockGroupedBarGraphData,
} from "data/mocks";
import { pageRoutes } from "data/routes";
import React from "react";
import { Color } from "utils/Color";

import { About } from "@/components/About";
import {
  GroupedBarGraphHorizontal,
  GroupedBarGraphVertical,
} from "@/components/GroupedBarGraph";
import { LineGraph } from "@/components/LineGraph";
import { PieChart } from "@/components/PieChart";
import { QuotationCarousel } from "@/components/QuotationCarousel";
import { Sections } from "@/components/Sections";
import {
  StackedBarGraphVertical,
  StackedBarGraphHorizontal,
} from "@/components/StackedBarGraph";
import { Timeline } from "@/components/Timeline";

import { BottomNav } from "../components/BottomNav";
import { CenterWrapper } from "../components/CenterWrapper";
import { ColorPalette } from "../components/ColorPalette";
import { WordCloud } from "../components/WordCloud";

import styles from "./playground.module.css";

export default function Home() {
  return (
    <div className={styles.page} suppressHydrationWarning>
      <h1>Playground</h1>
      <p>Show off your components here!</p>

      <ColorPalette />

      <h2>
        <code>Text Styles</code>
      </h2>
      <h1>h1 h1 h1 h1 h1 h1 h1 h1 h1 h1 h1 h1 h1 h1 h1 h1 h1 h1</h1>
      <h2>h2 h2 h2 h2 h2 h2 h2 h2 h2 h2 h2 h2 h2 h2 h2 h2 h2 h2</h2>
      <h3>h3 h3 h3 h3 h3 h3 h3 h3 h3 h3 h3 h3 h3 h3 h3 h3 h3 h3</h3>
      <h4>h4 h4 h4 h4 h4 h4 h4 h4 h4 h4 h4 h4 h4 h4 h4 h4 h4 h4</h4>
      <p>p p p p p p p p p p p p p p p p p p p p p p p p p p p p</p>
      <a href="#">a a a a a a a a a a a a a a a a a a a a a a a a a a a a</a>

      <h2>
        <code>{"<PieChart />"}</code>
      </h2>
      <div style={{ padding: "30px" }}>
        <PieChart data={mockPieData} width={800} labelWidth={215} />
      </div>

      <h2>
        <code>{"<BarGraphHorizontal />"}</code>
      </h2>
      <BarGraphHorizontal
        className={styles.barGraphDemo}
        data={mockCategoricalData}
        width={800}
        height={500}
        margin={{
          top: 25,
          bottom: 40,
          left: 150,
          right: 20,
        }}
      />

      <h2>
        <code>{"<BarGraphVertical />"}</code>
      </h2>
      <p>
        <code>{"<BarGraphVertical />"}</code> takes the same props as{" "}
        <code>{"<BarGraphHorizontal />"}</code>.
      </p>
      <BarGraphVertical
        className={styles.barGraphDemo}
        data={mockCategoricalData}
        width={800}
        height={500}
        margin={{
          top: 20,
          bottom: 80,
          left: 60,
          right: 20,
        }}
      />

      <h2>
        <code>{"<WordCloud />"}</code>
      </h2>
      <WordCloud
        data={moreMockCategoricalData.map((word) => ({
          text: word.key,
          value: word.value,
        }))}
      />

      <h2>
        <code>{"<StackedBarGraphVertical />"}</code>
      </h2>
      <StackedBarGraphVertical
        width={600}
        height={400}
        keys={mockStackedBarKeys}
        colorRange={[
          Color.primaryAccent,
          Color.secondaryAccentLight,
          Color.primaryAccentLighter,
        ]}
        data={mockStackedBarGraphData}
        margin={{
          top: 20,
          left: 28,
          right: 20,
          bottom: 20,
        }}
      />

      <h2>
        <code>{"<StackedBarGraphHorizontal />"}</code>
      </h2>
      <p>
        <code>{"<StackedBarGraphHorizontal />"}</code> takes the same props as{" "}
        <code>{"<StackedBarGraphVertical />"}</code>.
      </p>
      <StackedBarGraphHorizontal
        width={600}
        height={400}
        keys={mockStackedBarKeys}
        colorRange={[
          Color.primaryAccent,
          Color.secondaryAccentLight,
          Color.primaryAccentLighter,
        ]}
        data={mockStackedBarGraphData}
        margin={{
          top: 60,
          left: 20,
          right: 20,
          bottom: 40,
        }}
      />

      <h2>
        <code>{"<Timeline />"}</code>
      </h2>
      <Timeline data={mockTimelineData} />

      <h2>
        <code>{"<Textbox />"}</code>
      </h2>
      <CenterWrapper>
        <h1>Preface</h1>
        <p>
          The CS Class Profile consists of data relevant to CS, CFM, and CS/BBA
          students. These were combined with the knowledge that students in
          these programs tend to have similar experiences, as many of the same
          CS required courses are shared. In the standard co-op offering, CS and
          CFM take 4 years and 2 semesters to complete, while CS/BBA can take up
          to a full 5 years.
        </p>
        <p>
          Computer Science (and the others) is known to be a very prestigious
          program, and is very well known in Canada as well as across the world.
          For prospective students or anyone who is interested in learning more
          about what the students are like, this CS Class Profile will attempt
          to answer some of your questions, and you may even learn a thing or
          two you didn’t expect!
        </p>
        <p>
          The survey questions were approved by the Institutional Analysis &
          Planning, where all University of Waterloo stakeholders that are
          interested in conducting a non-academic research survey involving a
          large portion of the UWaterloo population are reviewed and approved.
          The entirety of the survey creation and data processing was done by
          the UW Computer Science Club, so please check us out if you enjoy what
          you see!
        </p>
      </CenterWrapper>

      <h2>
        <code>{"<LineGraph />"}</code>
      </h2>
      <LineGraph
        // eslint-disable-next-line @typescript-eslint/no-unsafe-assignment
        data={mockLineData}
        width={600}
        height={400}
        margin={{
          top: 20,
          bottom: 80,
          left: 30,
          right: 20,
        }}
        colorRange={[Color.primaryAccent, Color.secondaryAccentLight]}
      />

      <h2>
        <code>{"<Sections />"}</code>
      </h2>
      <Sections data={pageRoutes} />

      <h2>
        <code>{"<About />"}</code>
      </h2>
      <About />

      <h2>
        <code>{"<BoxPlot />"}</code>
      </h2>
      <BoxPlot
        width={600}
        height={400}
        data={mockBoxPlotData}
        margin={{
          top: 20,
          left: 20,
        }}
      />

      <h2>
        <code>{"<QuotationCarousel />"}</code>
      </h2>
      <div className={styles.quotationCarouselDemo}>
        <QuotationCarousel data={mockQuoteData} circleDiameter={0} />
        <QuotationCarousel
          data={mockQuoteDataLong}
          width={800}
          height={160}
          circleDiameter={180}
        />
      </div>

      <h2>
        <code>{"<BottomNav />"}</code>
      </h2>
      <BottomNav
        leftPage={pageRoutes.demographics}
        rightPage={pageRoutes.coop}
      ></BottomNav>

      <h2>
        <code>{"<GroupedBarGraphVertical />"}</code>
      </h2>
      <GroupedBarGraphVertical
        className={styles.barGraphDemo}
        data={mockGroupedBarGraphData}
        barColors={[Color.primaryAccentLight, Color.secondaryAccentLight]}
        barHoverColorsMap={{
          [Color.primaryAccentLight]: Color.primaryAccent,
          [Color.secondaryAccentLight]: Color.secondaryAccent,
        }}
        width={500}
        height={400}
        margin={{
          top: 20,
          bottom: 40,
          left: 50,
          right: 20,
        }}
      />

      <h2>
        <code>{"<GroupedBarGraphHorizontal />"}</code>
      </h2>
      <p>
        <code>{"<GroupedBarGraphHorizontal />"}</code> takes the same props as{" "}
        <code>{"<GroupedBarGraphVertical />"}</code>.
      </p>
      <GroupedBarGraphHorizontal
        className={styles.barGraphDemo}
        data={mockGroupedBarGraphData}
        barColors={[Color.primaryAccentLight, Color.secondaryAccentLight]}
        barHoverColorsMap={{
          [Color.primaryAccentLight]: Color.primaryAccent,
          [Color.secondaryAccentLight]: Color.secondaryAccent,
        }}
        width={600}
        height={400}
        margin={{
          top: 20,
          bottom: 40,
          left: 60,
          right: 20,
        }}
      />
    </div>
  );
}
