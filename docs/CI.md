## Setting up CI

- Follow instructions on [CSC cloud](https://docs.cloud.csclub.uwaterloo.ca/kubernetes/) to setup csccloud.
- Setup the repository to store your docker images on [CSC Harbour](https://docs.cloud.csclub.uwaterloo.ca/registry/). 
- Make sure to add harbour auth tokens to drone. Click the my profile page on harbour, and set HARBOUR_USERNAME and HARBOUR_PASSWORD on drone secrets to the username and cli secret from the harbour profile page.
- Clone the [server repo](https://git.csclub.uwaterloo.ca/snedadah/csc-webhooks), fill in desired links, update env file with the auth token, update STAGING_AUTH_TOKEN in drone to match. Run `npm start` to start server in the background.
- Possibly put a crontab to restart the script on [startup](https://stackoverflow.com/questions/12973777/how-to-run-a-shell-script-at-startup)

