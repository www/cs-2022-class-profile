/** @type {import('next').NextConfig} */
let basePath = process.env.NEXT_PUBLIC_BASE_PATH ? process.env.NEXT_PUBLIC_BASE_PATH : "";
const nextConfig = {
  basePath: basePath,
  assetPrefix: basePath + "/",

  publicRuntimeConfig : { basePath: basePath },

  // test comment
  reactStrictMode: true,
  trailingSlash: true,
  // This image loader supports `next export`, for optimizing next <Image /> tags
  images: {
    loader: 'akamai',
    path: basePath,
  },
}

module.exports = nextConfig
