FROM nginx
COPY ./out /usr/share/nginx/html

COPY staging-nginx.conf /etc/nginx/conf.d
RUN rm /etc/nginx/conf.d/default.conf